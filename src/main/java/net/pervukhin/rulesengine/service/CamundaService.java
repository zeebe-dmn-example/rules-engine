package net.pervukhin.rulesengine.service;

import net.pervukhin.rulesengine.configuration.DmnRegistry;
import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CamundaService {
    private DmnRegistry registry;

    private DmnEngine dmnEngine;

    @Autowired
    public CamundaService(DmnRegistry registry, DmnEngine dmnEngine) {
        this.registry = registry;
        this.dmnEngine = dmnEngine;
    }

    public DmnDecisionResult evaluateDecision(String decisionId, Map<String, Object> variables) {
        DmnDecisionResult decisionResult = dmnEngine.evaluateDecision(registry.getByKey(decisionId), variables);

        return decisionResult;
    }
}
