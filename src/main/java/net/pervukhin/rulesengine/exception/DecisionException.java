package net.pervukhin.rulesengine.exception;

public class DecisionException extends RuntimeException {
    public DecisionException(String s) {
        super(s);
    }
}
