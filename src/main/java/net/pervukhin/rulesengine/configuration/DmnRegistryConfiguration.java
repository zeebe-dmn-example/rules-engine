package net.pervukhin.rulesengine.configuration;

import org.camunda.bpm.dmn.engine.DmnEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;

@Configuration
public class DmnRegistryConfiguration {

    private final DmnEngine dmnEngine;

    private final ResourcePatternResolver resourceResolver;

    @Autowired
    public DmnRegistryConfiguration(DmnEngine dmnEngine, ResourcePatternResolver resourceResolver) {
        this.dmnEngine = dmnEngine;
        this.resourceResolver = resourceResolver;
    }

    @Bean
    public DmnRegistry dmnRegistry() throws IOException {
        return new DmnRegistry(dmnEngine, resourceResolver);
    }
}
