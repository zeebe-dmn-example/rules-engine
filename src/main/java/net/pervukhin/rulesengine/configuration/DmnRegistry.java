package net.pervukhin.rulesengine.configuration;

import com.google.common.io.Resources;
import org.camunda.bpm.dmn.engine.DmnDecision;
import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.model.dmn.Dmn;
import org.camunda.bpm.model.dmn.DmnModelInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.ResourcePatternResolver;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class DmnRegistry {
    private final static Logger logger = LoggerFactory.getLogger(DmnRegistry.class);
    private Map<String, DmnDecision> registry = new HashMap<>();
    private final DmnEngine dmnEngine;
    private final ResourcePatternResolver resourceResolver;

    public DmnRegistry(DmnEngine dmnEngine, ResourcePatternResolver resourceResolver) throws IOException {
        this.dmnEngine = dmnEngine;
        this.resourceResolver = resourceResolver;
        init();
    }

    public void init() throws IOException {
        Arrays.stream(resourceResolver.getResources("classpath:dmn-models/*.dmn"))
                .forEach(resource -> {
                    try {
                        logger.debug("loading model: {}", resource.getFilename());
                        final DmnModelInstance dmnModel = Dmn.readModelFromStream((InputStream) Resources
                                .getResource("dmn-models/" + resource.getFilename()).getContent());
                        dmnEngine.parseDecisions(dmnModel).forEach(decision -> {
                            logger.debug("Found decision with id '{}' in file: {}", decision.getKey(),
                                    resource.getFilename());
                            registry.put(decision.getKey(), decision);
                        });
                    } catch (IOException e) {
                        logger.error("Error parsing dmn: {}", resource, e);
                    }
        });
    }

    public DmnDecision getByKey(String key) {
        return registry.get(key);
    }
}
