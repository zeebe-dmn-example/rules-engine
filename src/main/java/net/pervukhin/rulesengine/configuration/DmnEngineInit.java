package net.pervukhin.rulesengine.configuration;

import org.camunda.bpm.dmn.engine.DmnEngine;
import org.camunda.bpm.dmn.engine.DmnEngineConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DmnEngineInit {
  
  @Bean
  public DmnEngine initDmnEngine() {
    return DmnEngineConfiguration.createDefaultDmnEngineConfiguration().buildEngine();
  }
  
}
