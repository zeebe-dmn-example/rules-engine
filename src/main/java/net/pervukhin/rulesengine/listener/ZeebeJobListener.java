package net.pervukhin.rulesengine.listener;

import io.zeebe.client.ZeebeClient;
import io.zeebe.client.api.response.ActivatedJob;
import net.pervukhin.rulesengine.exception.DecisionException;
import net.pervukhin.rulesengine.service.CamundaService;
import org.camunda.bpm.dmn.engine.DmnDecisionResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

@Service
public class ZeebeJobListener {
    private static final Logger logger = LoggerFactory.getLogger(ZeebeJobListener.class);
    public static final String RESULT_DECISION_FIELD = "result";
    public static String DECISION_ID_HEADER = "decisionRef";

    private final ZeebeClient zeebeClient;
    private final String jobWorker;
    private final CamundaService camundaService;

    @Autowired
    public ZeebeJobListener(ZeebeClient zeebeClient, CamundaService camundaService,
                            @Value("${zeebe-client.worker-name}") String jobWorker) {
        this.zeebeClient = zeebeClient;
        this.camundaService = camundaService;
        this.jobWorker = jobWorker;
    }

    @PostConstruct
    public void init() {
        logger.debug("Zeebe subscription");
        subscribeToDMNJob();
    }

    private void subscribeToDMNJob() {
        zeebeClient.newWorker().jobType(String.valueOf(jobWorker)).handler(
                (jobClient, activatedJob) -> {
                    logger.debug("processing DMN");
                    final String decisionId = readHeader(activatedJob, DECISION_ID_HEADER);
                    final Map<String, Object> variables = activatedJob.getVariablesAsMap();
                    DmnDecisionResult decisionResult = camundaService.evaluateDecision(decisionId, variables);
                    if (decisionResult.size() == 1) {
                        if (decisionResult.get(0).containsKey(RESULT_DECISION_FIELD)) {
                            variables.put(RESULT_DECISION_FIELD, decisionResult.get(0).get(RESULT_DECISION_FIELD));
                        }
                    } else {
                        throw new DecisionException("Нет результата решения.");
                    }

                    jobClient.newCompleteCommand(activatedJob.getKey())
                            .variables(variables)
                            .send()
                            .join();
                }
        ).open();
    }

    private String readHeader(ActivatedJob job, String customHeaderName) {
        String headerValue = (String) job.getCustomHeaders().get(customHeaderName);
        if (headerValue == null || headerValue.isEmpty()) {
            throw new RuntimeException(String.format("Missing header: '%d'", customHeaderName));
        }
        return headerValue;
    }
}
