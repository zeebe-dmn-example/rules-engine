FROM openjdk:8-jdk-alpine

COPY target/*.jar /opt/app/

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/opt/app/rules-engine-0.0.1.jar"]